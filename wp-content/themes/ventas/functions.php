<?php 

function  load_stylesheets(){

   
    wp_register_style('bootstrap_stylesheet',get_stylesheet_directory_uri() . '/css/shop-homepage.css',array(),false,"all");
    wp_register_style('template_stylesheet',get_stylesheet_directory_uri() . '/vendor/bootstrap/css/bootstrap.min.css',array(),false,"all");
   
   
    wp_enqueue_style('style', get_stylesheet_uri(), NULL , microtime() );
    wp_enqueue_style('bootstrap_stylesheet');
    wp_enqueue_style('template_stylesheet');

}
add_action("wp_enqueue_scripts", "load_stylesheets");

function aditional_features(){

    register_nav_menu('headerMenuLocation','Header Menu Location');

}

add_action("after_setup_theme","aditional_features");
?>